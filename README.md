# Front-End Homework
Homework to apply for Frontend Developer position at ---

## Getting Started

### Prerequisites
```
npm
```

### Installation
* Install dependencies
```
npm install
```
* Build
```
npm run build
```
* Serve
```
npm run dev-server
```

## Technologies used
* [React](https://reactjs.org/)
* [ES6](http://es6-features.org/)
* [Babel](http://babeljs.io/)
* [Webpack](https://webpack.github.io/)
* [Chart.js](http://www.chartjs.org/)
* [Scss](http://sass-lang.com/)
* [BEM](http://getbem.com/)

### Why I've chosen this technologies?
* First I was thinking to develop the app with plain JavaScript and Chart.js, but then I decided to use these technologies to build it for the following reason:
I've been learning React since 3 weeks, so I decided to use it on this Homework to test my skills and ES6, Babel and Webpack together, everything new for me.
* I've used chart.js for the first time and I haven't been able to build the styles exactly as in the image, so I've tried to do it similarly.
* I've also used BEM for an easier reading of the styles.

## Authors
* João Miller
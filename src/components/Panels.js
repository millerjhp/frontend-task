import React from 'react';
import Installations from './Installations';
import Panel from './Panel';
import Revenue from './Revenue';

export default class Panels extends React.Component {
  state = {
    revenueData: {},
    installationsData: {}
  };

  componentWillMount(){
    this.getRevenueData();
    this.getInstallationsData();
  }

  getRevenueData() {
    this.setState({
      revenueData:{
        labels: ['Week 48', 'Week 49', 'Week 50', 'Week 51', 'Week 52'],
        datasets: [
          {
            label: 'Net Comp',
            data: [600, 700, 800, 900, 300],
            borderColor: 'rgba(237, 110, 55,1)',
            pointBackgroundColor: 'rgba(237, 110, 55,1)',
            pointBorderColor: 'rgba(255,255,255,1)',
            pointBorderWidth: 4,
            pointRadius: 6,
            fill: false,
            ticks: {
              beginAtZero: false
            }
          },
          {
            label: 'AnalyzerHR',
            data: [400, 300, 500, 700, 200],
            borderColor: 'rgba(37, 158, 1, 1)',
            pointBackgroundColor: 'rgba(37, 158, 1, 1)',
            pointBorderColor: 'rgba(255,255,255,1)',
            pointBorderWidth: 4,
            pointRadius: 6,
            fill: false
          },
          {
            label: 'Question Right',
            data: [300, 100, 300, 300, 100],
            backgroundColor: 'rgba(21, 159, 199, 0.1)',
            borderColor: 'rgba(21, 159, 199, 1)',
            pointBackgroundColor: 'rgba(21, 159, 199, 1)',
            pointBorderColor: 'rgba(255,255,255,1)',
            pointBorderWidth: 4,
            pointRadius: 6,
            fill: '-2'
          }
        ]
      }
    });
  }

  getInstallationsData() {
    this.setState({
      installationsData: {
        labels: ['Week 48', 'Week 49', 'Week 50', 'Week 51', 'Week 52'],
        datasets: [
          {
            label: 'Net Comp',
            data: [6, 7, 8, 9, 3],
            backgroundColor: 'orange'
            
          },
          {
            label: 'AnalyzerHR',
            data: [4, 3, 5, 7, 2],
            backgroundColor: 'green'
          },
          {
            label: 'Question Right',
            data: [3, 1, 3, 3, 1],
            backgroundColor: 'lightblue'
          }
        ]
      }
    });
  }

  revenueClicked = () => {
    alert('Revenue clicked');
  };

  installationsClicked = () => {
    alert('Installations clicked');
  };

  render () {
    const { revenueData, installationsData } = this.state;
    return (
      <div className="panels">
        <div className = "container">
          <Panel
            buttonText="Revenue Analysis"
            buttonClicked={this.revenueClicked}
            icon="fa fa-usd"
            label="Revenue Stream"
            title="Revenue by Solution"
            chartData={revenueData}
          >
            <Revenue chartData={revenueData} />
          </Panel>
          <Panel 
            buttonText="View Installations"
            buttonClicked={this.installationsClicked}
            icon="fa fa-arrow-circle-down"
            label="Installations"
            title="Installations"
            chartData={installationsData}
          > 
            <Installations chartData={installationsData} />
          </Panel>
        </div>
      </div>
    );
  }
}
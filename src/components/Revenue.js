import React from 'react';
import {Line} from 'react-chartjs-2';

export default class Revenue extends React.Component {

  state = {
    chartData: this.props.chartData
  };

  render() {
    return (
      <div>
        <div className="chart">
          <Line
            data={this.props.chartData}
            options={{
              title: {
                display: true,
                fontSize: 12,
                position: 'top',
                text: 'Revenue per week'
              },
              legend: {
                display: true,
                position: 'bottom',
                labels: {
                  boxWidth: 10
                }
              },
              elements: {
                line: {
                  tension: 0
                }
              },
              maintainAspectRatio: false,
              scales: {
                xAxes: [{
                  display: true,
                  gridLines: false,
                  offset: true,
                  ticks: {
                    display: true,
                    fontColor: '#05415b'
                  }
                }],
                yAxes: [{
                  ticks: {
                    fontColor: '#05415b'
                  }
                }]
              }
            }}
            height={180}
          />
        </div>
      </div>
    );
  }
}
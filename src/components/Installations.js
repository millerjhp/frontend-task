import React from 'react';
import {Bar} from 'react-chartjs-2';

export default class Installations extends React.Component {

  state = {
    chartData: this.props.chartData
  };

  render() {
    return (
      <div>
        <div className="chart">
          <Bar
            data={this.state.chartData}
            options={{
              title: {
                display: true,
                fontSize: 12,
                position: 'top',
                text: 'Installations per day'
              },
              legend: {
                display: true,
                position: 'bottom',
                labels: {
                  boxWidth: 10
                }
              },
              maintainAspectRatio: false,
              scales: {
                xAxes: [{
                  display: true,
                  barPercentage: 1.0,
                  categoryPercentage: 0.5,
                  barThickness: 10,
                  gridLines: {
                    display: false,
                    offsetGridLines: true
                  },
                  offset: true,
                  ticks: {
                    display: true,
                    fontColor: '#05415b'
                  }
                }],
                yAxes: [{
                  ticks: {
                    fontColor: '#05415b'
                  }
                }]
              }
            }}
            height={190}
          />
        </div>
      </div>
    );
  }
}
import React from 'react';

export default class Panel extends React.Component {
  state = {
    chartData: this.props.chartData
  };

  dataSum = () => {
    const data = this.props.chartData.datasets; 
    const getData = [];
    const getNewData = [];
    data.forEach((dataset) => {
      getData.push(dataset.data);
    });
    getData.forEach((label) => {
      getNewData.push(label.reduce((prev, cur) => prev + cur));
    })
    //console.log(numbers[0].reduce((prev, cur) => prev + cur));
    return getNewData.reduce((prev, cur) => prev + cur);
  };

  render() {
    return (
      <div className="panel">
        <div className="panel__header">
          <div className="panel__icon">
            <div className={this.props.icon} />
          </div>
          <h2 className="panel__title">{this.props.title}</h2>
        </div>
        <div className="panel__body">
          {this.props.children}
          <div className="panel__info">
            <div className="panel__info-dataSum">
              {this.props.label == 'Revenue Stream' ? <p><span className="panel__info-dataSum--dollar">$</span>{this.dataSum()}</p> : <p>{this.dataSum()}</p>}
            </div>
            <p className="panel__info-label">{this.props.label}</p>
            <button
              className="button"
              onClick={this.props.buttonClicked}
            >
              {this.props.buttonText}
            </button>
          </div>
        </div>
      </div>
    );
  }
}
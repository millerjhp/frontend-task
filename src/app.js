import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css/normalize.css';
import './styles/styles.scss';
import Header from './components/Header';
import Panels from './components/Panels';

class App extends React.Component {

  render() {
    return (
      <div>
        <Header title='Frontend Developer Homework' />
        <Panels />
      </div>
      
    );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));